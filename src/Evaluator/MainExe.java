/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SGD;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.functions.VotedPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.lazy.KStar;
import weka.classifiers.lazy.LWL;
import weka.classifiers.misc.VFI;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.JRip;
import weka.classifiers.rules.OneR;
import weka.classifiers.rules.PART;
import weka.classifiers.rules.ZeroR;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.RandomTree;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author mohammad
 */
public class MainExe {

    public static String[] clsfNames = new String[]{
        "BayesNet", "DecisionStump", "DecisionTable", "IBk", "J48",
        "JRip", "KStar", "LibSVM", "Logistic", "LWL",
        "NaiveBayes", "OneR", "PART", "RandomTree", "REPTree",
        "SGD", "SimpleLogistic", "VFI", "VotedPerceptron", "ZeroR"
    };

    public static Classifier[] models = {
        new BayesNet(), new DecisionStump(), new DecisionTable(), new IBk(), new J48(),
        new JRip(), new KStar(), new LibSVM(), new Logistic(), new LWL(),
        new NaiveBayes(), new OneR(), new PART(), new RandomTree(), new REPTree(),
        new SGD(), new SimpleLogistic(), new VFI(), new VotedPerceptron(), new ZeroR()
    };

    public static double[][] expResult = null;
    public static String[] experiment = null;
    public static int expIdx = 0;

    private static int kFold = 10;
    private static Instances[] cvTrnData = new Instances[kFold];
    private static Instances[] cvTstData = new Instances[kFold];
    private static List<String> dataSets;
    private static List<String> exeCmd;
    private static String strPath, outPath;

    public static String CVClassifiers(Instances train) {
        String result = "Classifier \tMCC \tAccuracy \tPrecision \tFMeasure \tTP \tTN \tFP \tFN \n";
        for (int i = 0; i < models.length; i++) {
            Classifier currentClassifier = models[i];
            System.out.println("Testing: " + currentClassifier.getClass().getSimpleName());
            try {
                // evaluate classifier and print some statistics
                Evaluation eval = new Evaluation(train);
                eval.crossValidateModel(currentClassifier, train, 10, new Random(1));

                String resLine = currentClassifier.getClass().getSimpleName();

                resLine += ("\t" + eval.weightedMatthewsCorrelation()); // MCC
                resLine += ("\t" + eval.pctCorrect());                  // Accuracy
                resLine += ("\t" + eval.precision(0));                  // Precision
                resLine += ("\t" + eval.fMeasure(0));                   // FMeasure

                resLine += ("\t" + eval.numTruePositives(0));           // TP
                resLine += ("\t" + eval.numTrueNegatives(0));           // TN
                resLine += ("\t" + eval.numFalsePositives(0));          // FP
                resLine += ("\t" + eval.numFalseNegatives(0));          // FN
                resLine += "\n";

                result += resLine;

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static void TestClassifiers(Instances train, Instances Test, int exper) {
        int pos = 0;
        Classifier currentClassifier;

        for (int ci = 0; ci < clsfNames.length; ci++) {
            try {
                String[] options = null;

                currentClassifier = AbstractClassifier.makeCopy(models[ci]);
                // evaluate classifier and print some statistics
                currentClassifier.buildClassifier(train);
                Evaluation eval = new Evaluation(train);
                eval.evaluateModel(currentClassifier, Test);

                pos = exper * 2;
                expResult[ci][pos] = eval.pctCorrect();
                pos++;
                expResult[ci][pos] = eval.weightedMatthewsCorrelation();
            } catch (Exception ex) {
                Logger.getLogger(MainExe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void loadDatasetNames(String fileName) {
        try {
            BufferedReader br = null;
            dataSets = new ArrayList<String>();

            File fin = new File(fileName);
            br = new BufferedReader(new FileReader(fin));
            String line = null;
            String delims = "[ \t:]+";

            while ((line = br.readLine()) != null) {
                if (line.startsWith("Path:") == true) {
                    String[] tokens = line.split(delims);
                    strPath = tokens[1];
                } else if (line.startsWith("OutPath:") == true) {
                    String[] tokens = line.split(delims);
                    outPath = tokens[1];
                } else if (line.startsWith("#") == false) {
                    dataSets.add(line);
                }
            }
            br.close();
        } catch (FileNotFoundException ex) {
            System.err.println("File Not Found: " + fileName);
        } catch (IOException ex) {
            System.err.println("Unable ot Read File: " + fileName);
        }
    }

    public static void loadModels(String path) {
        Classifier[] models = new Classifier[clsfNames.length];
        String clsfPath = "";
        for (int mi = 0; mi < clsfNames.length; mi++) {
            try {
                clsfPath = path + clsfNames[mi] + ".model";
                Classifier cls = (Classifier) weka.core.SerializationHelper.read(clsfPath);
                models[mi] = cls;
            } catch (Exception ex) {
                System.err.println("Unable to Load: " + clsfPath);
                Logger.getLogger(MainExe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void exeCommandBuilder() {
        exeCmd = new ArrayList<String>();

        for (String strDS : dataSets) {
            if (strDS.length() > 0) {
                String trainFile = strPath + strDS + "/" + strDS + "-train.arff";
                String testFile = strPath + strDS + "/" + strDS + "-Test.arff";
                String modelPath = strPath + strDS + "/FullModel/";
                String cmdLine = "-t " + trainFile + " -T " + testFile + " -M " + modelPath;
                exeCmd.add(cmdLine);
            }
        }
    }

    public static void executeCommands() {
        int exper = 0;
        for (String strCMD : exeCmd) {
            try {
                System.out.println(strCMD);
                String[] args = strCMD.split(" ");

                // loads data and set class index
                String dataFile = Utils.getOption("t", args);
                Instances trn = ConverterUtils.DataSource.read(dataFile);
                trn.setClassIndex(trn.numAttributes() - 1);
                System.out.println("Train File: " + dataFile);

                dataFile = Utils.getOption("T", args);
                Instances tst = ConverterUtils.DataSource.read(dataFile);
                tst.setClassIndex(tst.numAttributes() - 1);
                System.out.println("Test File: " + dataFile);

                String modelPath = Utils.getOption("M", args);
                loadModels(modelPath);
                TestClassifiers(trn, tst, exper);
                
                exper++;
            } catch (Exception ex) {
                System.err.println("Unable to Load arff file...");
                Logger.getLogger(MainExe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void OutputResults() {
        System.out.print("Classifier");
        for (String strDS : dataSets) {
            System.out.print("\t" + strDS + "\t");
        }
        System.out.println();

        System.out.print("\t");
        for (String strDS : dataSets) {
            System.out.print("\tMCC\tACC");
        }
        System.out.println();

        for (int ci = 0; ci < clsfNames.length; ci++) {
            System.out.print(clsfNames[ci]);
            int tot = dataSets.size() * 2;

            for (int pos = 0; pos < tot; pos += 2) {
                System.out.print("\t" + expResult[ci][pos] + "\t" + expResult[ci][pos + 1]);
            }
            System.out.println();
        }

    }

    /**
     * @param args the command line arguments -t
     * /home/mohammad/Dataset/Thrombin/Cheng/thrombin-cheng-Weka.arff -T
     * /home/mohammad/Dataset/Thrombin/Cheng/test-Thrombin-Weka-R.arff -o
     * /home/mohammad/Dataset/Thrombin/Cheng/Cheng-Thrombin-4Feat-AllClsf.csv
     */
    public static void main(String[] args) throws Exception {
        // output usage
        if (args.length == 0) {
            System.err.println("\nUsage: java -jar EnsembleConsensus.jar -i <input file containing the settings>\n");
            System.exit(1);
        } else if (args[0].equals("-i")) {
            String fname = args[1];
            loadDatasetNames(fname);
            exeCommandBuilder();
            expResult = new double[clsfNames.length][dataSets.size() * 2];
            executeCommands();

            System.out.println("\nResults:\n\n");
            OutputResults();
        } else {
            System.err.println("\nUsage: java -jar EnsembleConsensus.jar-i <input file containing the settings>\n");
            System.exit(1);
        }
        System.out.println("Done!\n");
    }
}
