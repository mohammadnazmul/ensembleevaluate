/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ensemble;

import java.util.concurrent.Callable;
import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Utils;

/**
 *
 * @author mohammad
 */
public class ConsensusFusion implements Callable<Double> {

    protected final Classifier[] m_Classifiers;
    protected final int instanceIdx;
    protected final Instance data;
    
    public ConsensusFusion(Classifier[] m_Classifiers, int instanceIdx, Instance data) {
        this.m_Classifiers = m_Classifiers;
        this.instanceIdx = instanceIdx;
        this.data = data;
    }

    /**
     * Determine predictions for a single instance (defined in "instanceIdx").
     */
    public Double call() throws Exception {
        double[] classProbs = null;
        classProbs = new double[data.numClasses()];
        
        for (int baseClsfIdx = 0; baseClsfIdx < m_Classifiers.length; baseClsfIdx++) {
            Classifier baseClsf = (Classifier) m_Classifiers[baseClsfIdx];
            
            double[] curDist;
            curDist = baseClsf.distributionForInstance(data);
            for (int classIdx = 0; classIdx < curDist.length; classIdx++) {
                classProbs[classIdx] += curDist[classIdx];
            }
        }
        double vote;
        vote = Utils.maxIndex(classProbs); // consensus - for classification
        return vote;
    }
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Ensemble of Classifier : Consensus Fusion\n");
        buffer.append("-----------------\n");
        for (Classifier classifier : m_Classifiers) {
            buffer.append(classifier.toString());
        }
        buffer.append("\n-----------------\n");

        return buffer.toString();
    }
}
