/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ensemble;

import java.util.List;
import java.util.Random;
import java.util.Vector;
import weka.classifiers.Classifier;
import weka.classifiers.RandomizableMultipleClassifiersCombiner;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;

/**
 *
 * @author mohammad
 */
public class MajorityVoteTieCount extends RandomizableMultipleClassifiersCombiner {

    /**
     *
     */
//    private static final long serialVersionUID = -8996052328536347834L;
    protected Random m_Random;
    protected int tieCounter;

    @Override
    public void buildClassifier(Instances data) throws Exception {
        // can classifier handle the data?
        getCapabilities().testWithFail(data);

        // remove instances with missing class
        Instances newData = new Instances(data);
        newData.deleteWithMissingClass();
        m_Random = new Random(getSeed());

        for (Classifier classifier : m_Classifiers) {
            classifier.buildClassifier(data);
        }
    }

    /**
     * Classifies the given test instance.
     *
     * @param instance the instance to be classified
     * @return the predicted most likely class for the instance or
     * Instance.missingValue() if no prediction is made
     * @throws Exception if an error occurred during the prediction
     */
    public double classifyInstance(Instance instance) throws Exception {
        double result = 0.0;
        double[] dist;
        int index;

        dist = distributionForInstance(instance);
        if (instance.classAttribute().isNominal()) {
            index = Utils.maxIndex(dist);
            result = index;
        } else if (instance.classAttribute().isNumeric()) {
            result = dist[0];
        }

        return result;
    }

    /**
     * Classifies a given instance using the selected combination rule.
     *
     * @param instance the instance to be classified
     * @return the distribution
     * @throws Exception if instance could not be classified successfully
     */
    public double[] distributionForInstance(Instance instance) throws Exception {
        double[] result = new double[instance.numClasses()];
        result = distributionForInstanceMajorityVoting(instance);
        if (!instance.classAttribute().isNumeric() && (Utils.sum(result) > 0)) {
            Utils.normalize(result);
        }

        return result;
    }

    /**
     * Classifies a given instance using the Majority Voting combination rule.
     *
     * @param instance the instance to be classified
     * @return the distribution
     * @throws Exception if instance could not be classified successfully
     */
    protected double[] distributionForInstanceMajorityVoting(Instance instance) throws Exception {
        double[] probs = new double[instance.classAttribute().numValues()];
        double[] votes = new double[probs.length];

        for (int i = 0; i < m_Classifiers.length; i++) {
            probs = getClassifier(i).distributionForInstance(instance);
            int maxIndex = 0;
            for (int j = 0; j < probs.length; j++) {
                if (probs[j] > probs[maxIndex]) {
                    maxIndex = j;
                }
            }

            // Consider the cases when multiple classes happen to have the same probability
            for (int j = 0; j < probs.length; j++) {
                if (probs[j] == probs[maxIndex]) {
                    votes[j]++;
                }
            }
        }

        int tmpMajorityIndex = 0;
        for (int k = 1; k < votes.length; k++) {
            if (votes[k] > votes[tmpMajorityIndex]) {
                tmpMajorityIndex = k;
            }
        }

        // Consider the cases when multiple classes receive the same amount of votes
        Vector<Integer> majorityIndexes = new Vector<Integer>();
        for (int k = 0; k < votes.length; k++) {
            if (votes[k] == votes[tmpMajorityIndex]) {
                majorityIndexes.add(k);
                tieCounter++;
            }
        }
        
        // Resolve the ties according to a uniform random distribution
        int majorityIndex = majorityIndexes.get(m_Random.nextInt(majorityIndexes.size()));

        //set probs to 0
        probs = new double[probs.length];
        probs[majorityIndex] = 1; //the class that have been voted the most receives 1

        return probs;
    }

    /*
     * Sets the list of possible classifers to choose from.
     */
    public void setPreBuiltClassifiers(List<Classifier> arr_classifiers) {
        int idx = 0;
        for (Classifier cls : arr_classifiers) {
            m_Classifiers[idx] = cls;
            idx++;
        }
        tieCounter = 0;
    }
    
    public void setPreBuiltClassifiers(Classifier[] arr_classifiers) {       
        for (int idx=0; idx< arr_classifiers.length; idx++) {
            m_Classifiers[idx] = arr_classifiers[idx];
        }
        tieCounter = 0;
    }

    /*
     * Gets the list of possible classifers to choose from.
     */
    public Classifier[] getClassifiers() {
        return m_Classifiers;
    }

    /*
     * Gets the number of times the tie have been broken.
     */
    public int getTotalTie() {
        return tieCounter;
    }

    public Classifier getClassifier(int index) {
        return m_Classifiers[index];
    }

    /*
     * Returns combined capabilities of the base classifiers, i.e., the
     * capabilities all of them have in common.
     */
    public Capabilities getCapabilities() {
        Capabilities result;
        int i;

        result = (Capabilities) getClassifier(0).getCapabilities().clone();
        for (i = 1; i < getClassifiers().length; i++) {
            result.and(getClassifier(i).getCapabilities());
        }

        // set dependencies
        for (Capabilities.Capability cap : Capabilities.Capability.values()) {
            result.enableDependency(cap);
        }

        result.setOwner(this);

        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Ensemble of Classifier : Majority Vote\n");
        buffer.append("-----------------\n");
        for (Classifier classifier : m_Classifiers) {
            buffer.append(classifier.toString());
        }
        buffer.append("\n-----------------\n");

        return buffer.toString();
    }
}
